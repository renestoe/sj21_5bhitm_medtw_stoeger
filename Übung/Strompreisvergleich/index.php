<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.1/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-F3w7mX95PdgyTmZZMECAngseQB83DfGTowi0iMjiWaeVhAn4FJkqJByhZMI3AhiU" crossorigin="anonymous">
    <link rel="stylesheet" href="./style.css">
</head>
<body>
    <form class="form-container" method="post">
        <h2 class="title">Strompreisvergleich</h2>
        <label>kWh/Jahr</label><br>
        <input type="number" name="kWh"><br>
        <input type="submit" name="submit" value="Abschicken">
    </form>

    <h2 class="title">3 Angebote</h2>
    <table class="table">
        <thead>
        <tr>
            <th scope="col">Anbieter</th>
            <th scope="col">Wichtige Informationen</th>
            <th scope="col">Jahrespreis</th>
        </tr>
        </thead>
        <tbody>
            <?php
            $checkstart = "";
            $servername = "localhost";
            $username = "root";
            $password = "";
            $dbname = "provider";

            $pdo = new PDO("mysql:host=$servername;dbname=$dbname", $username, $password);
            $sql = "SELECT * FROM provider";
            $tmp = $pdo->query($sql);
            $result = $tmp->FetchAll(PDO::FETCH_ASSOC);

            $price = 0;

            if (isset($_POST['submit'])) {
                $kWH = $_REQUEST['kWh'];

                foreach ($result as $row) {
                    $provider = $row["Anbieter"];
                    $description = $row["beschreibung"];
                    $price1 = $row["Preis 1"];
                    $price2 = $row["Preis 2"];
                    $price3 = $row["Preis 3"];

                    if($kWH > 0 && $kWH <= 1000)
                        $price = $price1 * $kWH;
                    else if($kWH > 1000 && $kWH <= 3000)
                        $price = $price2 * $kWH;
                    else
                        $price = $price3 * $kWH;

                    echo "<tr>
                <td>$provider</td>
                <td>$description</td>
                <td>€ $price</td>
            </tr>";
                }
            }
            ?>
        </tbody>
    </table>

</body>
</html>