<?php get_header(); ?>
    <div id="main" class="row">
        <div id="content" class="col-lg-8 col-sm-8 col-md-8 col-xs-12">

            <div class="row">
                <?php if (have_posts())
                { while (have_posts())
                    { the_post(); ?>
                    <div class="col-lg-8 col-sm-8 col-md-8 col-xs-12">
                        <h1><?php the_title(); ?></h1>
                        <h4>Posted on <?php the_time('F jS, Y') ?></h4>
                        <p><?php the_content(__('(more...)')); ?></p>
                    </div>
                <?php }} else{ ?>
                    <p><?php _e('Sorry, no posts matched your criteria.'); ?></p>
                <?php } ?>
            </div>
        </div>
        <?php get_sidebar(); ?>
    </div>

<?php get_footer(); ?>

<!-- _e ist wie echo, nur dass es übersetzt wird, falls es sprachvorlagen gibt -->
