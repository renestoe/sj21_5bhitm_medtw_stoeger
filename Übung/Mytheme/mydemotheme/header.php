<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>htlkrems theme</title>
    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri().'/js/jquery.js'; ?>">
    </script>
    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri().'/js/jquery-ui.min.js'; ?>">
    </script>
    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri().'/js/bootstrap.js'; ?>">
    </script>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri().'/css/bootstrap.css'; ?>"> <!-- tells WordPress to load a style.css file that will handle the styling of the theme. -->
    <link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>"> <!--is a WordPress function that actually loads the stylesheet. -->
</head>
<body>

<div id="header" class="jumbotron">
    <div class="heading" style="padding-top: 19px; ">
        <h1>HEADER</h1>
       <?php
       wp_nav_menu( array(
           'container_class' => 'custom-menu-class' ) );
       ?>
   </div>

</div>

<div class="container">



