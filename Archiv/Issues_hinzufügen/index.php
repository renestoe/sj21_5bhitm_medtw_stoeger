<?php
$pdo = new PDO('mysql:host=localhost;dbname=trackstar', 'root', '');

?>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <title>Trackstar</title>
    <link rel="stylesheet" href="../dist/css/styles.css">
    <link rel="stylesheet" href="../node_modules/bootstrap-icons/font/bootstrap-icons.css">
<body>
<nav class="navbar">
    <a style="color:black;" href="index.php"><span class="navbar-brand mb-0 h1">Projektübersicht</span></a>
    <?php
    echo "<span>Benutzer: " . 'User1' . "</span>"
    ?>
</nav>

<main>
    <div class="tableFixHead">
        <table class="table table-striped">
            <thead class="thead-dark">
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Description</th>
                <th scope="col">Date</th>
                <th scope="col">Operations</th>
            </tr>
            </thead>
            <tbody>
            <?php
            $tmp = $pdo->query('SELECT * FROM projects');
            $result = $tmp->fetchAll(PDO::FETCH_ASSOC);

            foreach ($result as $row) {
                echo "<tr>
                        <td>" . $row['title'] . "</td>
                        <td>" . $row['description'] . "</td>
                        <td>" . $row['created_at'] . "</td>";
                echo "<td><a style='color:black;' href='delete.php?id=" . $row['project_id'] . "'><span class='icons'><i class='bi bi-trash'></i></span></a>
                       <a style='color:black;' href='index.php?edit=" . $row['project_id'] . "'><span class='icons'><i class='bi bi-pencil-fill'></i></span></a>
                      <a style='color:black;' href='index.php?issue=" . $row['project_id'] . "'><span class='icons'><i class='bi bi-card-list'></i></span></td></a>";
            }

            ?>
            </tbody>
        </table>
    </div>

    <?php
    if (isset($_GET['issue'])) {
    $issue = $_GET['issue'];
    $sql = 'SELECT * FROM projects where project_id=' . $issue;
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    while ($row = $stmt->fetch()) {
        echo "<h2 style='margin-top: 50px'>Issue List " . $row['title'] . "</h2>";
    }

    ?>
    <a href="index.php?add=<?php echo $issue; ?>" class="btn btn-outline-secondary btn-sm"><i
                class="bi bi-plus-square"></i> New Issue</button></a>
    <?php
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $description = isset($_POST['description']) ? $_POST['description'] : '';
        $category = isset($_POST['category']) ? $_POST['category'] : '';
        $projectId = isset($_POST['projectId']) ? $_POST['projectId'] : '';


        if (empty($description) || empty($category) || empty($projectId)) {
            echo '<script>alert("Alle Felder ausfüllen!");</script>';
        } else {
            $is = new PDO('mysql:host=localhost;dbname=trackstar', 'root', '');
            $sql = "INSERT INTO issues (issue_id, category, description, created_at ,project_id)
            VALUES (NULL,:category,:description,CURRENT_TIME(), :projectId)";
            $sth = $is->prepare($sql);
            $sth->execute(['category' => $category, 'description' => $description, 'projectId' => $projectId]);
        }
    }

    $sql = 'SELECT * FROM issues where project_id=' . $issue;
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    ?>
    <table class="table table-striped">
        <?php
        while ($row = $stmt->fetch()) {
            switch ($row['category']) {
                case 'ticket':
                    echo "<tr>
                                <td>Fehlerticket: " . $row['description'] . "</td>
                          </tr>";
                    break;

                case 'feature':
                    echo "<tr>
                                <td>Funktionalität: " . $row['description'] . "</td>
                          </tr>";
                    break;

                case 'task':
                    echo "<tr>
                                <td>Aufgabe: " . $row['description'] . "</td>
                          </tr>";
                    break;

                default:
                    break;
            }
        }

        echo "</table>";


        } elseif (isset($_GET['edit'])) {
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $title = isset($_POST['title']) ? $_POST['title'] : '';
                $description = isset($_POST['description']) ? $_POST['description'] : '';
                $id = isset($_POST['id']) ? $_POST['id'] : '';

                if (empty($description) || empty($title)) {
                    echo '<script>alert("Alle Felder ausfüllen!");</script>';

                } else {
                    $sql = "UPDATE projects SET title=?, description=? WHERE project_id=?";
                    $sth = $pdo->prepare($sql);
                    $sth->execute([$title, $description, $id]);

                }
            }
            $edit = $_GET['edit'];
            $sql = 'SELECT * FROM projects where project_id=' . $edit;
            $stmt = $pdo->prepare($sql);
            $stmt->execute();
            while ($row = $stmt->fetch()) {
                echo "<h2 style='margin-top: 50px'>Projekt " . $row['title'] . " bearbeiten</h2>";

                ?>
                <form action="index.php?edit=<?php echo $edit; ?>" method="POST">
                    <div class="form-group">
                        <label for="title">Name:</label>
                        <input name="title" id="title" type="text" class="form-control"
                               value="<?php echo $row['title']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="description">Beschreibung:</label>
                        <input type="text" class="form-control" id="description" name="description"
                               value="<?php echo $row['description']; ?>">
                    </div>
                    <input name="id" type="text" value="<?php echo $edit; ?>" style="display: none;">
                    <button type="submit" class="btn btn-secondary">Aktualisieren</button>
                    <a href="index.php" class="btn btn-secondary">Abbrechen</a>
                </form>
                <?php
            }

        } elseif (isset($_GET['add'])) {
            $add = $_GET['add'];
            $sql = 'SELECT * FROM projects where project_id=' . $add;
            $stmt = $pdo->prepare($sql);
            $stmt->execute();
            while ($row = $stmt->fetch()) {
                echo "<h2 style='margin-top: 50px'>Issue für " . $row['title'] . " hinzufügen</h2>";
            }
            ?>
            <form action="index.php?issue=<?php echo $add ?>" method="POST">
                <div class="input-group mb-3"
                <label for="category">Kategorie:</label>
                <select id="category" name="category" class="custom-select">
                    <option selected value="ticket">Fehlerticket</option>
                    <option value="feature">Funktionalität</option>
                    <option value="task">Aufgabe</option>
                </select>
                </div>
                <div class="form-group">
                    <label for="description">Beschreibung:</label>
                    <input type="text" class="form-control" id="description" name="description" value="">
                </div>
                <input type="text" name="projectId" value="<?php echo $add; ?>" style="display: none;">
                <button type="submit" class="btn btn-secondary">Submit</button>
            </form>
            <?php

        } else {
        }
        ?>


</main>

</body>
<script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
        integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
        crossorigin="anonymous"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
        integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
        crossorigin="anonymous"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
        integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
        crossorigin="anonymous"></script>
<script src="../dev/js/script.js"></script>
</html>