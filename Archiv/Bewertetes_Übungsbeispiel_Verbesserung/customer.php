<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css"
          integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous">
    <script src="https://code.jquery.com/jquery-3.3.1.slim.min.js"
            integrity="sha384-q8i/X+965DzO0rT7abK41JStQIAqVgRVzpbzo5smXKp4YfRvH+8abtTE1Pi6jizo"
            crossorigin="anonymous"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"
            integrity="sha384-UO2eT0CpHqdSJQ6hJty5KVphtPhzWj9WO1clHTMGa3JDZwrnQq4sF86dIHNDz0W1"
            crossorigin="anonymous"></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"
            integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM"
            crossorigin="anonymous"></script>
</head>
<body>

<?php
$pdo = new PDO('mysql:host=localhost;dbname=customer', 'root', '');

//Max Parameter in der URL übergeben
$max_credit = 0; //Ist der Standardwert
if (isset($_GET["max_credit"])) {   //Überprüft ob Wert Null ist
    $max_credit = $_GET["max_credit"];
}

$statement = $pdo->prepare("SELECT * FROM customers WHERE creditLimit <= $max_credit"); //Select Abfrage der auf die Datenbank
$statement->execute();
$row = $statement->fetchAll(); //Nimmt alle Zeilen aus der Datenbank
$count = $statement->rowCount();
?>

//Table
<table class="table">

    <thead class="thead-dark">
    <tr>
        <th scope="col">Name</th>
        <th scope="col">contactLastName</th>
        <th scope="col">contactFirstName</th>
        <th scope="col">Country</th>
        <th scope="col">Credit_Limit</th>
    </tr>
    </thead>

    <tbody>
    <?php
    //Daten werden durch die Schleife in die Tabelle geschrieben
    for ($x = 0; $x < $count; $x++) {
        echo '<tr id=' . $row[$x][0] . '>';
        echo '<td class="customerName">' . $row[$x][1] . '</td>';
        echo '<td>' . $row[$x][2] . '</td>';
        echo '<td>' . $row[$x][3] . '</td>';
        echo '<td>' . $row[$x][10] . '</td>';
        echo '<td>' . $row[$x][11] . '</td>';
        echo '</tr>';
    }
    ?>
    </tbody>
</table>

</body>
<script>
    //Wenn man auf den CustomerName drückt, wird dessen ID in die Konsole geschrieben
    $(".customerName").on('click', function () {
        console.log($(this).closest('tr').attr('id'));
    });
</script>
</html>