$(document).ready(function (){
    $('.delete-btn').click(function () { //Achtet darauf ob ein Klick auf ein Element, mit der Klasse delete-btn und löst bei Klick die Funktion aus
        let rowToDelete = $(this).attr("id"); //Bekommt id von Projekt
        $.ajax({ //Ajax wird gestartet
            url: "delete.php",
            method: "post",
            data: "id=" + rowToDelete,
            success: function (data) {
                if(data==1){
                    $('.row'+rowToDelete).remove(); //Sucht nach der Klasse row + project_id und löscht das Element mit der Klasse
                }
                else{
                    alert("Löschen war nicht erfolgreich!");
                }

            }
        });
    });
});