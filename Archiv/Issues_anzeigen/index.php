<?php
$pdo = new PDO("sqlite:trackstar.sqlite");
$sql = "SELECT * FROM projects";
$tmp = $pdo->query($sql);
$result = $tmp->fetchAll(PDO::FETCH_ASSOC);
?>



<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, us
          er-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <link rel="stylesheet" href="../dist/css/styles.css">
    <title>Document</title>
</head>
<body>


<main>
<div id="Header">
    <h1 class="inline">Projektübersicht</h1>
    <h3 class="user-right inline">Benutzer: User 1</h3>
</div>
    <br>

<div class="scrollbar">
<table class="table table-striped table-dark">
    <thead>
    <tr>
        <th  scope="col">Name</th>
        <th  scope="col">Description</th>
        <th  scope="col">Date</th>
        <th  scope="col">Operations</th>
    </tr>
    </thead>
    <tbody>
    <?php
    foreach ($result as $row){
        $tablerow = "";
        $tablerow .= '<td>' .$row["title"]. '</td>';
        $tablerow .= '<td>' .$row["description"]. '</td>';
        $tablerow .= '<td>' .$row["created_at"]. '</td>';
        $tablerow .= '<td>' ."<a href='delete.php?id=".$row['project_id']."'> <svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-trash' viewBox='0 0 16 16'>
                <path d='M5.5 5.5A.5.5 0 0 1 6 6v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm2.5 0a.5.5 0 0 1 .5.5v6a.5.5 0 0 1-1 0V6a.5.5 0 0 1 .5-.5zm3 .5a.5.5 0 0 0-1 0v6a.5.5 0 0 0 1 0V6z'/>
                <path fill-rule='evenodd' d='M14.5 3a1 1 0 0 1-1 1H13v9a2 2 0 0 1-2 2H5a2 2 0 0 1-2-2V4h-.5a1 1 0 0 1-1-1V2a1 1 0 0 1 1-1H6a1 1 0 0 1 1-1h2a1 1 0 0 1 1 1h3.5a1 1 0 0 1 1 1v1zM4.118 4L4 4.059V13a1 1 0 0 0 1 1h6a1 1 0 0 0 1-1V4.059L11.882 4H4.118zM2.5 3V2h11v1h-11z'/>
            </svg></a>

            <svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-pencil-fill' viewBox='0 0 16 16' style='margin-left: 20px'>
                <path d='M12.854.146a.5.5 0 0 0-.707 0L10.5 1.793 14.207 5.5l1.647-1.646a.5.5 0 0 0 0-.708l-3-3zm.646 6.061L9.793 2.5 3.293 9H3.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.5h.5a.5.5 0 0 1 .5.5v.207l6.5-6.5zm-7.468 7.468A.5.5 0 0 1 6 13.5V13h-.5a.5.5 0 0 1-.5-.5V12h-.5a.5.5 0 0 1-.5-.5V11h-.5a.5.5 0 0 1-.5-.5V10h-.5a.499.499 0 0 1-.175-.032l-.179.178a.5.5 0 0 0-.11.168l-2 5a.5.5 0 0 0 .65.65l5-2a.5.5 0 0 0 .168-.11l.178-.178z'/>
            </svg>

            <a href='index.php?issue=".$row['project_id']."'><svg xmlns='http://www.w3.org/2000/svg' width='16' height='16' fill='currentColor' class='bi bi-table' viewBox='0 0 16 16' style='margin-left: 20px'>
                <path d='M0 2a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v12a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V2zm15 2h-4v3h4V4zm0 4h-4v3h4V8zm0 4h-4v3h3a1 1 0 0 0 1-1v-2zm-5 3v-3H6v3h4zm-5 0v-3H1v2a1 1 0 0 0 1 1h3zm-4-4h4V8H1v3zm0-4h4V4H1v3zm5-3v3h4V4H6zm4 4H6v3h4V8z'/>
            </svg></a>". '</td>';

        echo "<tr>" . $tablerow . "</tr>";


    }

    ?>


    </tbody>
</table>
</div>
    <?php
    if(isset($_GET['issue'])){
        $sql = "SELECT * FROM projects where project_id=".$_GET['issue'];
        $result = $pdo->query($sql);

        while ($row=$result->fetch(PDO::FETCH_ASSOC)){
            echo "<h2>".$row['title']."</h2>";
        }
    $sql = 'SELECT * FROM issues where project_id=' . $_GET['issue'];
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    ?>
    <table class="table table-striped table-dark">
        <?php
        while($row = $stmt->fetch()){
            switch ($row['category']){
                case 'ticket':
                    echo "<tr>
                                <td>Fehlerticket: ".$row['description']."</td>
                          </tr>";
                    break;

                case 'feature':
                    echo "<tr>
                                <td>Funktionalität: ".$row['description']."</td>
                          </tr>";
                    break;

                case 'task':
                    echo "<tr>
                                <td>Aufgabe: ".$row['description']."</td>
                          </tr>";
                    break;

                default:
                    break;
            }
        }
    }
    ?>
</main>


















<script src="https://code.jquery.com/jquery-3.5.1.slim.min.js"
        integrity="sha384-DfXdz2htPH0lsSSs5nCTpuj/zy4C+OGpamoFVy38MVBnE+IbbVYUew+OrCXaRkfj"
        crossorigin="anonymous">
</script>

<script src="https://cdn.jsdelivr.net/npm/bootstrap@4.5.3/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-ho+j7jyWK8fNQe+A12Hb8AhRq26LrZ/JpcUGGOn+Y7RsweNrtN/tE3MoK7ZeZDyx"
        crossorigin="anonymous">
</script>

</body>
</html>