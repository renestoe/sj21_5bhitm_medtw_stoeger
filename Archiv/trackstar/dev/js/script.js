$(document).ready(function () {
    $.getJSON( "getTable.php", function(json) {
        console.log(json);
        json.forEach(function (element) {
            $("#projects").append("<tr class='row"+ element.project_id + "'>" + "" +
                "<td>" + element.title + "</td>" +
                "<td>" + element.description + "</td>" +
                "<td>" + element.created_at + "</td>" +
                "<td>" +
                "<span id='"+ element.project_id +"' class='delete-btn' style='color:black; cursor: pointer;' class='icons'><i class='bi bi-trash'></i></span>" +
                "<a style='color:black;' href='index.php?edit=" + element.project_id  + "'><span class='icons'><i class='bi bi-pencil-fill'></i></span></a>" +
                "<a style='color:black;' href='index.php?issue=" + element.project_id +"'><span class='icons'><i class='bi bi-card-list'></i></span></a>" +
                "</td>" +
                "</tr>");
        })
    }).then(function() {

        $('.delete-btn').click(function () {
            let rowToDelete = $(this).attr("id");
            $.ajax({
                url: "delete.php",
                method: "post",
                data: "id=" + rowToDelete,
                success: function (data) {
                    if(data==1){
                        $('.row' + rowToDelete).remove();
                    } else {
                        alert("Löschen war nicht erfolgreich!");
                    }
                }
            })
        })
    })
});