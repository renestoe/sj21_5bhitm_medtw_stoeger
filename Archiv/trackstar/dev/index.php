<?php
$pdo = new PDO('mysql:host=localhost;dbname=trackstar', 'root', '');

session_start();
if(isset($_POST['username']) && isset($_POST['password'])) {
    $sql = 'select * from users where username="' . $_POST['username'] . '"';
    $result = $pdo->query($sql);
    $user = "";
    $pass = "";


    while ($row = $result->fetch()) {
        if(!empty($row)){
            $pass = $row['password'];
            $user = $row['username'];
        }else{
            $pass = null;
            $user = null;
        }
    }
    if ($_POST['password'] == $pass && $_POST['username']==$user) {
        $_SESSION['username'] = $user;
    }else{
        echo '<div class="alert alert-danger" role="alert">
                Benutzername oder Passwort ist falsch!
              </div>';
    }
}


?>
<!DOCTYPE html>
<html lang="de">
<head>
    <meta charset="UTF-8">
    <title>Trackstar</title>
    <link rel="stylesheet" href="../dist/css/styles.css">
    <link rel="stylesheet" href="../node_modules/bootstrap-icons/font/bootstrap-icons.css">
<body>
<?php if (isset($_SESSION['username']) && !empty($_SESSION['username'])){?>
<nav class="navbar">
    <a style="color:black;" href="index.php"><span class="navbar-brand mb-0 h1">Projektübersicht</span></a>
    <?php
    echo "<a style='color:black;' href='logout.php'><span>Benutzer: " . $_SESSION['username'] . "</span></a>"
    ?>
</nav>

<main>
    <div class="tableFixHead">
        <table class="table table-striped">
            <thead class="thead-dark">
            <tr>
                <th scope="col">Name</th>
                <th scope="col">Description</th>
                <th scope="col">Date</th>
                <th scope="col">Operations</th>
            </tr>
            </thead>
            <tbody id="projects">
            <?php
//            $tmp = $pdo->query('SELECT * FROM projects');
  //          $result = $tmp->fetchAll(PDO::FETCH_ASSOC);
//
  //          foreach ($result as $row) {
//
  //              $tableContent = "<tr class='row". $row['project_id'] ."'>";
    //            $tableContent .= "<td>" . $row['title'] . "</td>";
      //          $tableContent .= "<td>" . $row['description'] . "</td>";
        //        $tableContent .= "<td>" . $row['created_at'] . "</td>";
          //      $tableContent .= "<td>
            //                        <span id='". $row['project_id'] ."' class='delete-btn' style='color:black; cursor: pointer;' class='icons'><i class='bi bi-trash'></i></span>
              //                      <a style='color:black;' href='index.php?edit=" . $row['project_id'] . "'><span class='icons'><i class='bi bi-pencil-fill'></i></span></a>
                //                    <a style='color:black;' href='index.php?issue=" . $row['project_id'] . "'><span class='icons'><i class='bi bi-card-list'></i></span></a>
                  //                </td>
                    //              </tr>";

//                echo $tableContent;
  //          }
            ?>
            </tbody>
        </table>
    </div>

    <?php
    if (isset($_GET['issue'])) {
    $issue = $_GET['issue'];
    $sql = 'SELECT * FROM projects where project_id=' . $issue;
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    while ($row = $stmt->fetch()) {
        echo "<h2 style='margin-top: 50px'>Issue List " . $row['title'] . "</h2>";
    }

    ?>
    <a href="index.php?add=<?php echo $issue; ?>" class="btn btn-outline-secondary btn-sm"><i
                class="bi bi-plus-square"></i> New Issue</button></a>
    <?php
    if ($_SERVER['REQUEST_METHOD'] == 'POST') {
        $description = isset($_POST['description']) ? $_POST['description'] : '';
        $category = isset($_POST['category']) ? $_POST['category'] : '';
        $projectId = isset($_POST['projectId']) ? $_POST['projectId'] : '';


        if (empty($description) || empty($category) || empty($projectId)) {
            echo '<script>alert("Alle Felder ausfüllen!");</script>';
        } else {
            $sql = "INSERT INTO issues (issue_id, category, description, created_at ,project_id)
            VALUES (NULL,:category,:description,CURRENT_TIME(), :projectId)";
            $sth = $pdo->prepare($sql);
            $sth->execute(['category' => $category, 'description' => $description, 'projectId' => $projectId]);
        }
    }

    $sql = 'SELECT * FROM issues where project_id=' . $issue;
    $stmt = $pdo->prepare($sql);
    $stmt->execute();
    ?>
    <table class="table table-striped">
        <?php
        while ($row = $stmt->fetch()) {
            switch ($row['category']) {
                case 'ticket':
                    echo "<tr>
                                <td>Fehlerticket: " . $row['description'] . "</td>
                          </tr>";
                    break;

                case 'feature':
                    echo "<tr>
                                <td>Funktionalität: " . $row['description'] . "</td>
                          </tr>";
                    break;

                case 'task':
                    echo "<tr>
                                <td>Aufgabe: " . $row['description'] . "</td>
                          </tr>";
                    break;

                default:
                    break;
            }
        }

        echo "</table>";


        } elseif (isset($_GET['edit'])) {
            if ($_SERVER['REQUEST_METHOD'] == 'POST') {
                $title = isset($_POST['title']) ? $_POST['title'] : '';
                $description = isset($_POST['description']) ? $_POST['description'] : '';
                $id = isset($_POST['id']) ? $_POST['id'] : '';

                if (empty($description) || empty($title)) {
                    echo '<script>alert("Alle Felder ausfüllen!");</script>';

                } else {
                    $sql = "UPDATE projects SET title=?, description=? WHERE project_id=?";
                    $sth = $pdo->prepare($sql);
                    $sth->execute([$title, $description, $id]);

                }
            }
            $edit = $_GET['edit'];
            $sql = 'SELECT * FROM projects where project_id=' . $edit;
            $stmt = $pdo->prepare($sql);
            $stmt->execute();
            while ($row = $stmt->fetch()) {
                echo "<h2 style='margin-top: 50px'>Projekt " . $row['title'] . " bearbeiten</h2>";

                ?>
                <form action="index.php?edit=<?php echo $edit; ?>" method="POST">
                    <div class="form-group">
                        <label for="title">Name:</label>
                        <input name="title" id="title" type="text" class="form-control"
                               value="<?php echo $row['title']; ?>">
                    </div>
                    <div class="form-group">
                        <label for="description">Beschreibung:</label>
                        <input type="text" class="form-control" id="description" name="description"
                               value="<?php echo $row['description']; ?>">
                    </div>
                    <input name="id" type="text" value="<?php echo $edit; ?>" style="display: none;">
                    <button type="submit" class="btn btn-secondary">Aktualisieren</button>
                    <a href="index.php" class="btn btn-secondary">Abbrechen</a>
                </form>
                <?php
            }

        } elseif (isset($_GET['add'])) {
            $add = $_GET['add'];
            $sql = 'SELECT * FROM projects where project_id=' . $add;
            $stmt = $pdo->prepare($sql);
            $stmt->execute();
            while ($row = $stmt->fetch()) {
                echo "<h2 style='margin-top: 50px'>Issue für " . $row['title'] . " hinzufügen</h2>";
            }
            ?>
            <form action="index.php?issue=<?php echo $add ?>" method="POST">
                <div class="input-group mb-3"
                <label for="category">Kategorie:</label>
                <select id="category" name="category" class="custom-select">
                    <option selected value="ticket">Fehlerticket</option>
                    <option value="feature">Funktionalität</option>
                    <option value="task">Aufgabe</option>
                </select>
                </div>
                <div class="form-group">
                    <label for="description">Beschreibung:</label>
                    <input type="text" class="form-control" id="description" name="description" value="">
                </div>
                <input type="text" name="projectId" value="<?php echo $add; ?>" style="display: none;">
                <button type="submit" class="btn btn-secondary">Submit</button>
            </form>
            <?php


        }
        } else {
            ?>
            <h1 id="heading">Trackstar</h1>
            <div class="form-centering">
                <form class="form-horizontal" action="index.php" method="post">
                    <div class="form-group row" style="width: 500px;">
                        <label class="control-label col-sm-3" for="username">Benutzername</label>
                        <div class="col-sm-9">
                            <input name="username" type="text" class="form-control" id="username">
                        </div>
                    </div>
                    <div class="form-group row" style="width: 500px;">
                        <label class="control-label col-sm-3" for="password">Passwort</label>
                        <div class="col-sm-9">
                            <input name="password" type="password" class="form-control" id="password">
                        </div>
                    </div>
                    <button type="submit" class="btn btn-secondary">Anmelden</button>
                </form>
            </div>

            <?php
        }
        ?>


</main>

</body>

<script src="https://code.jquery.com/jquery-3.6.0.js"
        integrity="sha256-H+K7U5CnXl1h5ywQfKtSj8PCmoN9aaq30gDh27Xc0jk="
        crossorigin="anonymous"></script>
<script>
    $(document).ready(function () {
        $.getJSON( "getTable.php", function(json) {
            console.log(json);
            json.forEach(function (element) {
                $("#projects").append("<tr class='row"+ element.project_id + "'>" + "" +
                    "<td>" + element.title + "</td>" +
                    "<td>" + element.description + "</td>" +
                    "<td>" + element.created_at + "</td>" +
                    "<td>" +
                    "<span id='"+ element.project_id +"' class='delete-btn' style='color:black; cursor: pointer;' class='icons'><i class='bi bi-trash'></i></span>" +
                    "<a style='color:black;' href='index.php?edit=" + element.project_id  + "'><span class='icons'><i class='bi bi-pencil-fill'></i></span></a>" +
                    "<a style='color:black;' href='index.php?issue=" + element.project_id +"'><span class='icons'><i class='bi bi-card-list'></i></span></a>" +
                    "</td>" +
                    "</tr>");
            })
        }).then(function() {

            $('.delete-btn').click(function () {
                let rowToDelete = $(this).attr("id");
                $.ajax({
                    url: "delete.php",
                    method: "post",
                    data: "id=" + rowToDelete,
                    success: function (data) {
                        if(data==1){
                            $('.row' + rowToDelete).remove();
                        } else {
                            alert("Löschen war nicht erfolgreich!");
                        }
                    }
                })
            })
        })
    });
</script>
</html>